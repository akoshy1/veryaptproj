import requests
import time

#parses table and stores data
def parse_and_store(tableLink):
    data = requests.get(tableLink).text
    units = data.split('<tr ')
    units = units[1:] 

    for unit in units: 
          
        fields  = unit.split('<td>')
        fields  = fields[1:]

        #find unit number for each unit and stores in unitNum
        unitNum = fields[0].strip(r'span class=\"mobile-only\">Unit: </span> </td>')

        #find availability
        dateStart = fields[1].find("Date('")
        tempDate = fields[1]
        tempDate = tempDate[dateStart:]
        dateEnd = tempDate.find("');")
        tempDate = tempDate[:dateEnd]
        tempDate = tempDate.strip("Date('")
        date = time.strptime(tempDate, "%m/%d/%Y")

        #find number of bed
        tempBed = fields[2].strip(r'<span class=\"mobile-only\">Beds: </span><nobr> </nobr></td>') 
        if(tempBed == "STU"):
            bed = 0
        else:
            bed = int(tempBed[0])

        #find number of baths
        tempBath = fields[3].strip(r"<span class=\mobile-only\">Baths:" 
                + "</span><nobr<script>document.write(''.replace('', ''));</script> BA</nobr></td>")
        tempBath = tempBath[:3] 
        bath = float(tempBath)

        #floorplan link
        linkStart = fields[4].find("http") 
        tempLink = fields[4]
        tempLink = tempLink[linkStart:]
        linkEnd = tempLink.find("' ")
        floorPlan = tempLink[:linkEnd] 

        #find pricing
        dollar = fields[6].find("$")
        tempPrice = fields[6]
        tempPrice = tempPrice[dollar:]
        tempPrice = tempPrice.strip(r" </td>")
        tempPrice = tempPrice.replace("$", '') 
        price = int(tempPrice)

        print ("%s %d/%d/%d %d %.1f %d %s" % (unitNum, date.tm_mon, 
            date.tm_mday, date.tm_year, bed, bath, price, floorPlan))


#finds link to table
def find_link(siteLink):

    data = requests.get(siteLink).text
    linkStart = data.find("<iframe src=")
    data = data[linkStart:]
    data = data.strip(r'<iframe src="')
    linkEnd = data.find('"')
    data = data[:linkEnd]


    table = requests.get(data).text
    table = table.split("<div ")
    table = table[1]
    linkStart = table.find('src="')
    table = table[linkStart:]
    table = table.strip('srce="')
    linkEnd = table.find('"')
    tableLink = table[:linkEnd]

    #feeds final link to parse_and_store function
    parse_and_store(tableLink)


find_link("http://www.3130nlakeshoredrive.com/availability/")

