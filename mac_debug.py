import requests
import datetime


def runMac(city, building): 
       
    inc = 0
    apartments = {}

    for inc in range(0, 60, 15):
        
        date = datetime.datetime.today() + datetime.timedelta(inc)
        CITY = city
        BUILDING = building
        POST_FIELDS = {
            'action': 'MA_ViewApartmentsController',
            'method': 'searchDatabase',
            'data': [{
                'apexType': 'MA_ViewApartmentsController.SearchRequest',
                'PageNumber': 4,
                'SearchType': 'Unit',
                'City': CITY,
                'MoveInDate': date.strftime("%m/%d/%Y"),
                'BuildingOrStreet': BUILDING
            }],
            'type': 'rpc',
            'tid': 2,
            'ctx': {
                'vid': '066i0000004Y954',
                'ns': '',
                'ver': 31
            }
        }

        GET_FIELDS = {
            'city': CITY,
            'buildingOrStreet': BUILDING
        }
        GET_LOCATION = 'https://www.macapartments.com/search'
        POST_LOCATION = 'https://www.macapartments.com/apexremote'

        GET_RESULT = requests.get(
            GET_LOCATION, params = GET_FIELDS, verify = False).text
        CSRF_TOKEN = GET_RESULT.split('csrf":"', 1)[1].split('"', 1)[0]
        POST_FIELDS['ctx']['csrf'] = CSRF_TOKEN
        HEADERS = {
            'Referer': GET_LOCATION
        }

        data = requests.post(
            POST_LOCATION, json = POST_FIELDS, verify = False, headers = HEADERS).json()


        size = len(data[0]['result']['Units'])
        units = data[0]['result']['Units'] 

        for unit in units: 

            unitNum = unit['UnitNumber']
            availability = date.strftime("%m/%d/%Y")
            bed = int(unit['UnitBedrooms'])
            bath = float(unit['UnitBathroms'])
            price = int((unit['UnitRent']).replace(",", ""))
 
            if unitNum not in apartments:
                apartments[unitNum] =  [availability, bed, bath, price]
 

    for key, value in apartments.items() :
        print (key, value)
    

def propertyInfo(siteLink): 

    city = siteLink.split("city=", 1)[1].split("&")[0]
    building = siteLink.split("OrStreet=")[1].replace("%20", " ")
    runMac(city, building)


propertyInfo("https://www.macapartments.com/search?&city=a0Pi0000001FxWzEAK&moveInDate=06/12/2017&buildingOrStreet=1606%20E.%20Hyde%20Park%20Blvd")
